import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Project } from '../_models/project';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};
@Injectable({ providedIn: 'root' })

export class TaskService {
    constructor(private http: HttpClient) {}
    // get_projectbyid(projectId: string) {
    //     return this.http.post<any>(environment.apiUrl + '/project/get_project_by_id/', { projectId })
    //     .pipe(map(data => {
    //         if (data.result) {
    //             return data.data;
    //         } else {
    //             return data.errors[0];
    //         }
    //     }));
    // }
    // get_projects(userId: string) {
    //     const url = '/project/get_projects/?userId=' + String(userId);
    //     console.log(environment.apiUrl + url);
    //     return this.http.get<any>(String(environment.apiUrl + url))
    //     .pipe(map(data => {
    //         console.log(data);
    //         if (data.result) {
    //             return data;
    //         } else {
    //             return data.errors[0];
    //         }
    //     }));
    // }
    create_edit_task(userId: string, taskName: string, taskDescription:string, TaskImage:string, taskId: number, projectId: number) {
        return this.http.post<any>(String(environment.apiUrl + '/task/create_task/'), { userId, taskName, taskDescription, TaskImage, taskId, projectId})
        .pipe(map(data => {
                // if (data.result) {
                    return data;
                // } else {
                    // return data.errors[0];
                // }
            }, err => {
                return err;
            }
        ));
    }
    create_edit_subtask(userId: string, taskName: string, taskDescription:string, TaskImage:string, taskId: number, subtaskId: number) {
        return this.http.post<any>(String(environment.apiUrl + '/task/create_task/'), { userId, taskName, taskDescription, TaskImage, taskId, subtaskId})
        .pipe(map(data => {
                // if (data.result) {
                    return data;
                // } else {
                    // return data.errors[0];
                // }
            }, err => {
                return err;
            }
        ));
    }
    delete_task(userId: string, taskId: number = null, projectId: number) {
        return this.http.post<any>(String(environment.apiUrl + '/task/delete_task/'), { userId, taskId, projectId})
        .pipe(map(data => {
                return data;
            }, err => {
                return err;
            }
        ));
    }
}

import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { Project } from '../_models/project';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};
@Injectable({ providedIn: 'root' })

export class ProjectService {
    constructor(private http: HttpClient) {}
    get_projectbyid(projectId: string) {
        return this.http.post<any>(environment.apiUrl + '/project/get_project_by_id/', { projectId })
        .pipe(map(data => {
            // login successful if there's a jwt token in the response
            if (data.result) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                return data.data;
            } else {
                return data.errors[0];
            }
        }));
    }
    get_projects(userId: string) {
        // httpOptions.headers.set('userId', userId);
        const url = '/project/get_projects/?userId=' + String(userId);
        console.log(environment.apiUrl + url);
        return this.http.get<any>(String(environment.apiUrl + url))
        .pipe(map(data => {
            console.log(data);
            // login successful if there's a jwt token in the response
            if (data.result) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                return data;
            } else {
                return data.errors[0];
            }
        }));
    }
    create_edit_project(userId: string, projectName: string, projectDescription: string, projectImage: string, projectId: number = null) {
        return this.http.post<any>(String(environment.apiUrl + '/project/create_project/'), { userId, projectName, projectDescription, projectImage, projectId})
        .pipe(map(data => {
                // if (data.result) {
                    return data;
                // } else {
                    return data.errors[0];
                // }
            }, err => {
                return err;
            }
        ));
    }
    delete_project(userId: string, projectId: number = null) {
        return this.http.post<any>(String(environment.apiUrl + '/project/delete_project/'), { userId, projectId})
        .pipe(map(data => {
                return data;
            }, err => {
                return err;
            }
        ));
    }
}

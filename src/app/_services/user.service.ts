import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { User } from '../_models/user';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { 
        const header = new HttpHeaders().set('access-control-allow-origin',"http://localhost:8080/");
    }

    // getAll() {
    //     return this.http.get<User[]>(environment.apiUrl+'/users');
    // }

    getById(id: number) {
        return this.http.get(environment.apiUrl + '/users/${id}');
    }

    register(user: any) {
        return this.http.post(environment.apiUrl + '/register/', user);
    }

    update(user: any) {
        return this.http.put(environment.apiUrl + '/users/${user.id}', user);
    }

    // delete(id: number) {
    //     return this.http.delete(environment.apiUrl+'/users/${id}');
    // }
}

// import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
// import { BehaviorSubject, Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment.prod';
// import { User } from '../_models/user';
// import { Router } from '@angular/router';

// @Injectable({ providedIn: 'root' })
// export class AuthenticationService {
//     private currentUserSubject: BehaviorSubject<any>;
//     public currentUser: Observable<User>;

//     constructor(private http: HttpClient, private router: Router) {
//         if (JSON.parse(localStorage.getItem('currentUser'))) {
//             this.currentUserSubject = new BehaviorSubject < any > (JSON.parse(localStorage.getItem('currentUser')));
//         } else {
//             this.currentUserSubject = new BehaviorSubject < any > ({user: {username: 'Anonymous'} } );
//         }
//         this.currentUser = this.currentUserSubject.asObservable();
//     }
//     public changeUserData(user: any) {
//         this.currentUserSubject.next(user);
//     }
//     public get currentUserValue(): any {
//         return this.currentUserSubject.value;
//     }

//     login(email: string, password: string) {
//         console.log(environment.apiUrl + '/login/');
//         return this.http.post<any>(environment.apiUrl + '/login/', { email, password })
//             .pipe(map(data => {
//                 console.log(data);
//                 if (data.user && data.token) {
//                     localStorage.setItem('currentUser', JSON.stringify(data));
//                     this.changeUserData(data);
//                 }

//                 return data;
//             }));
//     }
//     refresh_token(refresh: string) {
//         return this.http.post<any>(environment.apiUrl + '/token/refresh/', { refresh })
//             .pipe(map(data => {
//                 console.log(data);
//                 if (data.access) {
//                     const localUser = JSON.parse(localStorage.getItem('currentUser'));
//                     localUser.token = data.access;
//                     localStorage.setItem('currentUser', JSON.stringify(localUser));
//                     console.log(localUser.token);
//                     return localUser.token;
//                 } else {
//                     return data.errors;
//                 }
//             }));
//     }
//     verify_token(token: string) {
//         console.log(token);
//         return this.http.post<any>(environment.apiUrl + '/token/verify/', { token })
//             .pipe(map(data => {
//                 console.log(data);
//                 if (data.hasOwnProperty('code')) {
//                     return data;
//                 }
//             }, error => {
//                 console.log(error);
//             }));
//     }
//     logout(userId: string, token: string) {
//         this.currentUserSubject.next({user: {username: 'Anonymous'} });
//         localStorage.removeItem('currentUser');
//     }
// }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue() {
        return this.currentUserSubject.value;
    }

    login(email: string, password: string) {
        console.log(environment.apiUrl + '/login/');
        return this.http.post<any>(environment.apiUrl + '/login/', { email, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }
    refresh_token(refresh: string) {
        return this.http.post<any>(environment.apiUrl + '/token/refresh/', { refresh })
            .pipe(map(data => {
                console.log(data);
                if (data.access) {
                    const localUser = JSON.parse(localStorage.getItem('currentUser'));
                    localUser.token = data.access;
                    localStorage.setItem('currentUser', JSON.stringify(localUser));
                    console.log(localUser.token);
                    return localUser.token;
                } else {
                    return data.errors;
                }
            }));
    }
    verify_token(token: string) {
        console.log(token);
        return this.http.post<any>(environment.apiUrl + '/token/verify/', { token })
            .pipe(map(data => {
                console.log(data);
                if (data.hasOwnProperty('code')) {
                    return data;
                }
            }, error => {
                console.log(error);
            }));
    }
    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
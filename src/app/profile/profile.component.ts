import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserService } from '../_services/user.service';
import { AuthenticationService } from '../_services/authentication.service';
import { User } from '../_models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loginForm: FormGroup;
  username = false;
  loading = false;
  submitted = false;
  // returnUrl: string;
  constructor(
    private userService: UserService, 
    private authenticationService: AuthenticationService, 
    private alertService: AlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', ],
      mobile: ['', ],
      designation: ['', ],
      userTypeChoice: ['', ],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
    });
    this.userService.getById(this.authenticationService.currentUserValue.id)
    .pipe(first())
    .subscribe(
        data => {},
        err =>{}
    )
  }
  updateDetails(){

  }
}

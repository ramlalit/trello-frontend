import { User } from './user';
export class Task {
    taskId: string;
    name: string;
    description: string;
    userAssigned: User;
    isDone = false;
    isClicked = false;
    isClosed = false;
}
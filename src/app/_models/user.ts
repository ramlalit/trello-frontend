export class User {
    id: number;
    username: string;
    password: string;
    email: string;
    mobile: string;
    designation: string;
    userTypeChoice: string;
    name: string;
    token?: string;
}

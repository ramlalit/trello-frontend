import { User } from './user';
import { Task } from './task';
export class Project {
    projectId: string;
    name: string;
    taskList: [Task];
    description: string;
    userAssigned: User;
    isDone = false;
    isClosed = false;
    isClicked = false;
    isDeleted = false;
}

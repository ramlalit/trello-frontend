import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../_models/project';
import { Task } from '../_models/task';
import { first } from 'rxjs/operators';
import { TaskService } from '../_services/task.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-subtask',
  templateUrl: './subtask.component.html',
  styleUrls: ['./subtask.component.css']
})
export class SubtaskComponent implements OnInit {
  @Input() taskObject: any;
  @Input() projectObject: any;
  selectedTask = {};
  display = 'none';
  userId = JSON.parse(localStorage.getItem('currentUser')).user.id;

  constructor(private alertService: AlertService, private taskService: TaskService) { }

  ngOnInit() {
  }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  show(e: any, taskId: string) {
    console.log(taskId);
    if (taskId) {
      console.log(taskId);
      this.taskObject.subtaskList.forEach(task => {
        if (task.id === taskId) {
          console.log(taskId);
          task.isClicked = true;
        }
      });
    }
  }
  hide(data, subtask_id) {
    // console.log(subtask_id);
    // console.log(data)
    // console.log(this.taskObject)
    if (data) {
      this.taskObject.subtaskList.forEach(task => {
        if (task.name) {
          task.isClicked = false;
          if (task.id === subtask_id) {
            this.taskService.create_edit_subtask(this.userId, task.name, data.task_description, data.task_image, this.taskObject.id, subtask_id)
            .pipe(first())
            .subscribe(
              data => {
                this.taskObject.subtaskList = data.data;
              }, err => {
                task.isClicked = true;
              }
            );
          }
        } else {
          task.isClicked = true;
        }
      });
    }
  }
  // hide(e: any, taskId: string) {
  //   if (taskId.length) {
  //     this.projectObject.taskList.forEach(task => {
  //       if (String(task.taskId) === taskId) {
  //         task.isClicked = false;
  //       }
  //     });
  //   }
  // }
  openModal(task: Task) {
    this.selectedTask = task;
    this.display = 'block';
  }
  onCloseHandled() {
    this.selectedTask = new Task();
    this.display = 'none';
  }
  removeTask(e: any, index: number) {
    console.log(index);
    if (index) {
      this.taskObject.subtaskList.forEach(task => {
        if (task.id === index) {
          if (task.isClicked) {
            const i = this.taskObject.subtaskList.indexOf(task);
            this.taskObject.subtaskList.splice(i, 1);
          } else {
            this.taskService.delete_task(this.userId, task.id, this.taskObject.id)
            .pipe(first())
            .subscribe(
              data => {
                // if (data.result === 1) {
                  console.log(data);
                  this.taskObject.subtaskList = data.data;
                // }
              }
            );
          }
        }
      });
    }
  }
  addsubTask() {
    let indexNumber = 0;
    console.log(this.taskObject.subtaskList.length);
    if (this.taskObject.subtaskList.length>0) {
      console.log(this.taskObject.subtaskList[this.taskObject.subtaskList.length - 1].id);
      indexNumber = Number(this.taskObject.subtaskList[(this.taskObject.subtaskList.length - 1)].id) + 1;
      // indexNumber = (this.padLeft(str(indexNumber), '0', 6));
    } else {
      indexNumber = 1;
    }
    console.log(indexNumber);
    this.taskObject.subtaskList.push({
      isClicked: true,
      id: '',
      name : '',
      isDone: false,
      isClosed: false,
      description: '',
      userAssigned: JSON.parse(localStorage.getItem('currentUser')).user
    });
  }

}

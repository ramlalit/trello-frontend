import { Component, OnInit, Input } from '@angular/core';
import { Project } from '../_models/project';
import { Task } from '../_models/task';
import { first } from 'rxjs/operators';
import { TaskService } from '../_services/task.service';
import { AlertService } from '../_services/alert.service';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  @Input() projectObject: any;
  selectedTask = {};
  display = 'none';
  userId = JSON.parse(localStorage.getItem('currentUser')).user.id;
  // tasks = this.projectObject.tasks;
  constructor(private alertService: AlertService, private taskService: TaskService) { }

  ngOnInit() { }

  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }

  show(e: any, taskId: string) {
    console.log(taskId);
    if (taskId) {
      console.log(taskId);
      this.projectObject.taskList.forEach(task => {
        if (task.id === taskId) {
          console.log(taskId);
          task.isClicked = true;
        }
      });
    }
  }
  hide(data, task_id) {
    console.log(task_id);
    if (data) {
      this.projectObject.taskList.forEach(task => {
        if (task.name) {
          task.isClicked = false;
          if (task.id === task_id) {
            this.taskService.create_edit_task(this.userId, task.name, data.task_description, data.task_image, task_id, this.projectObject.id)
            .pipe(first())
            .subscribe(
              data => {
                this.projectObject.taskList = data.data;
              }, err => {
                task.isClicked = true;
              }
            );
          }
        } else {
          task.isClicked = true;
        }
      });
    }
  }
  // hide(e: any, taskId: string) {
  //   if (taskId.length) {
  //     this.projectObject.taskList.forEach(task => {
  //       if (String(task.taskId) === taskId) {
  //         task.isClicked = false;
  //       }
  //     });
  //   }
  // }
  openModal(task: Task) {
    this.selectedTask = task;
    this.display = 'block';
  }
  onCloseHandled() {
    this.selectedTask = new Task();
    this.display = 'none';
  }
  removeTask(e: any, index: number) {
    console.log(index);
    if (index) {
      this.projectObject.taskList.forEach(task => {
        if (task.id === index) {
          if (task.isClicked) {
            const i = this.projectObject.taskList.indexOf(task);
            this.projectObject.taskList.splice(i, 1);
          } else {
            this.taskService.delete_task(this.userId, task.id, this.projectObject.id)
            .pipe(first())
            .subscribe(
              data => {
                // if (data.result === 1) {
                  console.log(data);
                  this.projectObject.taskList = data.data;
                // }
              }
            );
          }
        }
      });
    }
  }
  addTask() {
    let indexNumber = 0;
    // console.log(this.projectObject.taskList.length);
    if (this.projectObject.taskList.length) {
      // console.log(this.projectObject.taskList[this.projectObject.taskList.length - 1].id);
      indexNumber = Number(this.projectObject.taskList[(this.projectObject.taskList.length - 1)].id) + 1;
      // indexNumber = (this.padLeft(str(indexNumber), '0', 6));
    } else {
      indexNumber = 1;
    }
    console.log(indexNumber);
    this.projectObject.taskList.push({
      isClicked: true,
      id: '',
      name : '',
      isDone: false,
      isClosed: false,
      description: '',
      userAssigned: JSON.parse(localStorage.getItem('currentUser')).user
    });
  }
}

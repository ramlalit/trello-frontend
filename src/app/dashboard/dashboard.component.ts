import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
// import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  user: any;
  constructor(private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    console.log(this.user);
  }

}

import { Component, HostBinding, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services/authentication.service';
import { User } from './_models/user';
import { first } from 'rxjs/operators';
import { AlertService } from './_services/alert.service';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { OverlayContainer} from '@angular/cdk/overlay';
@Component({ selector: 'app-root', templateUrl: 'app.component.html' })
export class AppComponent {

    title = 'workManagementApp';
    // currentUser: User;
    // isDarkTheme: any;
    // token: string;
    // // @ViewChild(DashboardComponent) dashboardComponent;
    // constructor(
    //     private router: Router,
    //     private authenticationService: AuthenticationService,
    //     public alertService: AlertService
    // ) {
//         if (this.currentUser) {
//             this.authenticationService.verify_token(this.currentUser.token)
//             .pipe(first())
//             .subscribe(
//             data => {
//                 console.log(data);

//             }, err => {
//                 console.log(err);
//                 this.authenticationService.refresh_token(JSON.parse(localStorage.getItem('currentUser')).refresh_token)
//                 .pipe(first())
//                 .subscribe(
//                     data => {
//                         console.log(data);
//                         this.router.navigate(['/']);
//                     }, err => {
//                         console.log(err);
//                     }
//                     );
//             }
//             );
//         }
//     }

//     changeTheme(value): void {
//         this.isDarkTheme = value;
//     }
//     logout() {
//         console.log('Logging Out');
//         // this.token = JSON.parse(localStorage.getItem('currentUser')).token;
//         this.authenticationService.logout(String(this.currentUser.id), this.currentUser.token);
//         // .pipe(first())
//         // .subscribe(
//         //     data => {
//         this.currentUser = null;
//         localStorage.removeItem('currentUser');
//         // this.authenticationService.currentUserValue.empty<User>();
//         this.router.navigate(['/login']);
//         this.alertService.success('You\'ve been logged out successfully!!');
//         //     },
//         // );
//     }
//     // ngAfterViewInit() {
//     //     this.currentUser = this.dashboardComponent.user;
//     // }
//     ngOnInit() {
//         this.authenticationService.currentUser.subscribe(x => {
//             console.log(x);
//             this.currentUser = x;
//             console.log(this.currentUser);
//             }
//         );
//     }
// }
    currentUser: any;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}

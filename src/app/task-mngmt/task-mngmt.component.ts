'use strict';
import { Component, OnInit } from '@angular/core';
// import { Project } from '../_models/project';
import { HttpClient } from '@angular/common/http';
import { ProjectService } from '../_services/project.service';
import { AlertService } from '../_services/alert.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-task-mngmt',
  templateUrl: './task-mngmt.component.html',
  styleUrls: ['./task-mngmt.component.css']
})
export class TaskMngmtComponent implements OnInit {
  projects: any = [];
  userId = JSON.parse(localStorage.getItem('currentUser')).user.id;
  public selectedProject = {};
  // {
  //   isClicked: false,
  //   projectId: '',
  //   name: '',
  //   tasks: []
  // };
  constructor(
    private alertService: AlertService,
    private projectService: ProjectService,

    ) { }

  // projects = [
  //   {
  //     isClicked: false,
  //     projectId: '000001',
  //     name: 'project 1',
  //     tasks: [
  //       {
  //         isClicked: false,
  //         taskId: '000001',
  //         name: 'task 1',
  //         isDone: false
  //       }
  //     ]
  //   },
  //   {
  //     isClicked: false,
  //     projectId: '000002',
  //     name: 'project 2',
  //     tasks: [
  //       {
  //         isClicked: false,
  //         taskId: '000002',
  //         name: 'task 2',
  //         isDone: true
  //       },
  //       {
  //         isClicked: false,
  //         taskId: '000003',
  //         name: 'task 3',
  //         isDone: false
  //       },
  //     ]
  //   },
  //   {
  //     isClicked: false,
  //     projectId: '000003',
  //     name: 'project 3',
  //     tasks: [
  //       {
  //         isClicked: false,
  //         taskId: '000004',
  //         name: 'task 4',
  //         isDone: false
  //       },
  //       {
  //         isClicked: false,
  //         taskId: '000005',
  //         name: 'task 5',
  //         isDone: false
  //       },
  //       {
  //         isClicked: false,
  //         taskId: '000006',
  //         name: 'task 6',
  //         isDone: false
  //       },
  //       {
  //         isClicked: false,
  //         taskId: '000007',
  //         name: 'task 7',
  //         isDone: false
  //       },
  //     ]
  //   },
  // ];
  show(e: any, projectId: number) {
    // console.log(projectId, 'line 95');
    if (projectId) {
      // console.log(projectId, 'line 97');
      this.projects.forEach(project => {
        if (project.id === projectId) {
          // console.log(projectId, 'line 100');
          project.isClicked = true;
        }
      });
    }
  }
  removeProject(e: any, index: number) {
    console.log(index);
    if (index) {
      this.projects.forEach(project => {
        if (project.id === index) {
          if (project.isClicked) {
            const i = this.projects.indexOf(project);
            this.projects.splice(i, 1);
          } else {
            this.projectService.delete_project(this.userId, project.id)
            .pipe(first())
            .subscribe(
              data => {
                // if (data.result === 1) {
                  console.log(data);
                  this.projects = data.data;
                // }
              }
            );
          }
        }
      });
    }
  }
  hide(data, project_id) {
    console.log(project_id)
    if (data) {
      this.projects.forEach(project => {
        if (project.name) {
          project.isClicked = false;
          if (project.id === project_id) {
            this.projectService.create_edit_project(this.userId, project.name, data.project_description, data.project_image, project_id)
            .pipe(first())
            .subscribe(
              data => {
                this.projects = data.data;
              }, err => {
                project.isClicked = true;
              }
            );
          }
        } else {
          project.isClicked = true;
        }
      });
    }
  }
  padLeft(text: string, padChar: string, size: number): string {
    return (String(padChar).repeat(size) + text).substr( (size * -1), size) ;
  }
  // $('#projectModel');
  addCard() {
    let indexNumber = 0;
    console.log(this.projects.length);
    if (this.projects.length) {
      console.log(this.projects[this.projects.length - 1].id);
      indexNumber = Number(this.projects[(this.projects.length - 1)].id) + 1;
      // indexNumber = (this.padLeft(str(indexNumber), '0', 6));
    } else {
      indexNumber = 1;
    }
    console.log(indexNumber);
    this.projects.push({
      isClicked: true,
      id: '',
      name : '',
      tasks: []
    });
  }
  ngOnInit() {
    this.projectService.get_projects(this.userId)
    .pipe(first())
    .subscribe(
      data => {
        // if (data.result) {
          this.projects = data.data;
        // } else {
          // this.alertService.error(data.errors[0]);
        // }
        // return data;
      },
      error => {
        console.log(error);
        // this.alertService.error(error);
      }
    );
    // console.log(this.projects);
  }
}

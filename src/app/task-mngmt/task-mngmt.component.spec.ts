import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskMngmtComponent } from './task-mngmt.component';

describe('TaskMngmtComponent', () => {
  let component: TaskMngmtComponent;
  let fixture: ComponentFixture<TaskMngmtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskMngmtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskMngmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

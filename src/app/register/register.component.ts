import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService } from '../_services/alert.service';
import {UserService } from '../_services/user.service';
import { AuthenticationService } from '../_services/authentication.service';

@Component({templateUrl: 'register.component.html'})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;
    loading = false;
    submitted = false;
    checkMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        // console.log(this.authenticationService.currentUserValue);
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }
    checkPwd(formObject: any) {
        if (formObject.password === formObject.repassword) {
            this.checkMessage = 'Both Passwords match';
        } else {
            this.checkMessage = 'Both Passwords don\'t match';
            // this.f.repassword.errors.required = true;
            // this.f.password.errors.required = true;
        }
        return this.checkMessage;
    }
    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            username: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(6)]],
            repassword: ['', [Validators.required, Validators.minLength(6)]],
            email: ['', [Validators.required] ],
            designation: ['', ],
            userTypeChoice: ['', ],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.register(this.registerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}

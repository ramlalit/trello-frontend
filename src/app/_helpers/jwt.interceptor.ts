import { catchError, filter, take, switchMap, finalize } from 'rxjs/operators';

import { Injectable } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpSentEvent, HttpUserEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';

import { AuthenticationService } from '../_services/authentication.service';
import { AlertService } from '../_services/alert.service';

// @Injectable()
// export class JwtInterceptor implements HttpInterceptor {
//     constructor(private authenticationService: AuthenticationService) {}

//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         // add authorization header with jwt token if available
//         const currentUser = JSON.parse(localStorage.getItem('currentUser'));
//         if (currentUser && currentUser.token) {
//             request = request.clone({
//                 setHeaders: {
//                     Authorization: 'Bearer ' + currentUser.token
//                 }
//             });
//         }

//         return next.handle(request);
//     }
// }


@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private authService: AuthenticationService, private alertService: AlertService) { }
    userSubject = JSON.parse(localStorage.getItem('currentUser'));
    isRefreshingToken = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
//     // tslint:disable-next-line: max-line-length
// tslint:disable-next-line: max-line-length
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {

        // tslint:disable-next-line: max-line-length
        if (JSON.parse(localStorage.getItem('currentUser'))) {
            request = this.addTokenToRequest(request, JSON.parse(localStorage.getItem('currentUser')).token);
        }
        return next.handle(request).pipe(catchError(err => {
            if (err instanceof HttpErrorResponse && err.status === 401) {
                return this.handle401Error(request, next);
            } else {
                const error = err.error.message || err.detail || err.statusText;

                // if (err.errors) {
                //     error = err.errors[0];
                // }
                this.alertService.error(error);
                return throwError(error);
            }
        }));
    }

    private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
        return request.clone({ setHeaders: { Authorization: 'Bearer ' + token}});
    }

    private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
//     }
// }
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;

            // Reset here so that the following requests wait until the token
            // comes back from the refreshToken call.
            this.tokenSubject.next(null);

            return this.authService.refresh_token(JSON.parse(localStorage.getItem('currentUser')).refresh_token)
            .pipe(
                switchMap((token) => {
                    console.log(token);
                    if (token) {
                        this.tokenSubject.next(token);
                        this.userSubject.token = token;
                        localStorage.setItem('currentUser', JSON.stringify(this.userSubject));
                        return next.handle(this.addTokenToRequest(request, this.userSubject.token));
                    }

                    return this.authService.logout() as any;
                }),
                catchError(err => {
                    return this.authService.logout() as any;
                }),
                finalize(() => {
                this.isRefreshingToken = false;
                })
            );
        } else {
            this.isRefreshingToken = false;

            return this.tokenSubject
            .pipe(filter(token => token != null),
                take(1),
                switchMap(token => {
                return next.handle(this.addTokenToRequest(request, token));
            }));
        }
    }
}
